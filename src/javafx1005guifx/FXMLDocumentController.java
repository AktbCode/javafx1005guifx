/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafx1005guifx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 *
 * @author AktbCode
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private ImageView CloseBtn;
    
    @FXML
    private Button Btn1, Btn2;
    
    @FXML
    private Pane Page1, Page2;
    @FXML
    private void handleCloseButtonAction(MouseEvent event) {
        if (event.getSource() == CloseBtn){
            System.exit(0);
        }
    }
    
    @FXML
    private void handleButtonAction(MouseEvent event){
        if (event.getSource() == Btn1){
            Page1.toFront();
        }
        if (event.getSource() == Btn2){
            Page2.toFront();
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
